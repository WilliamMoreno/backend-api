from django.db import models


class TipoContribuyente(models.Model):
    tipo_contribuyente = models.CharField(max_length=40, default='')

    class Meta:
        db_table = 'tipos_contribuyente'


class Contribuyente(models.Model):
    nit = models.IntegerField(primary_key=True)
    dv = models.IntegerField()
    nombre_razon_social = models.CharField(max_length=255, default='')
    direccion = models.CharField(max_length=255, default='')
    telefono = models.CharField(max_length=30, default='')
    token = models.IntegerField()
    email = models.EmailField()
    tipo_contribuyente = models.ForeignKey(
        TipoContribuyente, models.SET_NULL, null=True)

    class Meta:
        db_table = 'contribuyentes'


class Aprobaciones(models.Model):
    nit_contribuyente = models.ForeignKey(Contribuyente, models.CASCADE)
    resolucion = models.CharField(max_length=255, default='')
    fecha_aprobacion = models.DateField()
    fecha_vencimiento = models.DateField()
    num_inicial = models.IntegerField()
    num_final = models.IntegerField()
    prefijo = models.CharField(max_length=255, default='')

    class Meta:
        db_table = 'aprobaciones'


class EstadoFactura(models.Model):
    estado = models.CharField(max_length=30, default='')

    class Meta:
        db_table = 'estados_factura'


class Factura(models.Model):
    numero_factura = models.IntegerField()
    prefijo = models.CharField(max_length=255, default='')
    numero_completo = models.CharField(max_length=255, default='')
    nit_emisor = models.ForeignKey(Contribuyente, models.CASCADE)
    nit_receptor = models.CharField(max_length=50)
    fecha_factura = models.DateField()
    valor_factura = models.DecimalField(max_digits=15, decimal_places=2)
    valor_iva = models.DecimalField(max_digits=15, decimal_places=2)
    estado = models.ForeignKey(EstadoFactura, models.SET_NULL, null=True)

    class Meta:
        db_table = 'facturas'
