from rest_framework import serializers
from .models import Factura, Contribuyente, EstadoFactura

class ContribuyenteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Contribuyente
        fields = ('nit', 'nombre_razon_social', 'telefono')

class EstadoFacturaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EstadoFactura
        fields = ('estado',)

class FacturaSerializer(serializers.HyperlinkedModelSerializer):
    
    nit_emisor = ContribuyenteSerializer()
    estado = EstadoFacturaSerializer()
    class Meta:
        model = Factura
        fields = ('numero_completo','numero_factura','prefijo','nit_emisor', 'nit_receptor', 'estado', 'fecha_factura', 'valor_factura')

