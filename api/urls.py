from rest_framework import routers
from django.urls import path
from django.conf.urls import url, include
from .views import FacturaViewList
router = routers.SimpleRouter()
#router.register(r'factura', FacturaViewSet)     

urlpatterns = [
    path('', include(router.urls)),
    path('', FacturaViewList.as_view(), name="factura"),
]
