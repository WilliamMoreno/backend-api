import django_filters.rest_framework
from rest_framework import viewsets, generics
from rest_framework import filters
from .models import Factura
from .serializers import FacturaSerializer

# Create your views here.


class FacturaViewList(generics.ListAPIView):
    queryset = Factura.objects.all()
    serializer_class = FacturaSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['numero_completo', 'nit_emisor', 'nit_receptor']

    # def get_queryset(self):
    # return Factura.objects.filter()
